package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void firstFrameShouldBeOneAndFive() throws BowlingException {
		Frame frame = new Frame(1,5);
		Game game = new Game();
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(frame, game.getFrameAt(0));
		}
	
	@Test
	public void scoreShouldBeEightyOne() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(81, game.calculateScore());
		}
	
	@Test
	public void scoreShouldBeEightyEightAfterSpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(88, game.calculateScore());
		}
	
	@Test
	public void scoreShouldBeNinetyFourAfterStrike() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(94, game.calculateScore());
		}
	
	@Test
	public void scoreShouldBeOneHundredAndThreeAfterStrikeAndSpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(103, game.calculateScore());
		}
		
	@Test
	public void scoreShouldBeOneHundredAndTwelveAfterTwoStrike() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(112, game.calculateScore());
		}
	
	@Test
	public void scoreShouldBeNinetyEightAfterTwoSpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
			
		assertEquals(98, game.calculateScore());
		}
	
	@Test
	public void firstBonusThrowShouldBeSeven() throws BowlingException {
		Game game = new Game();
		int firstBonusThrow = 7;
		game.setFirstBonusThrow(firstBonusThrow);
		
		assertEquals(7, game.getFirstBonusThrow());
		}
	
	@Test
	public void scoreShouldBeNinetyAfterSpareAsLastFrameWithBonusThrowOfSeven() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(7);
			
		assertEquals(90, game.calculateScore());
		}
	
	@Test
	public void secondBonusThrowShouldBeTwo() throws BowlingException {
		Game game = new Game();
		int secondBonusThrow = 2;
		
		game.setSecondBonusThrow(secondBonusThrow);
		
		assertEquals(2, game.getSecondBonusThrow());
		}
	
	@Test
	public void scoreShouldBeNinetyTwoAfterStrikeAsLastFrameWithBonusThrowOfSevenAndTwo() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
			
		assertEquals(92, game.calculateScore());
		}
	
	@Test
	public void scoreShouldBeThreeHundredIfBestScore() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
			
		assertEquals(300, game.calculateScore());
		}

	
}
