package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	@Test
	public void firstThrowShouldBeTwo() throws Exception{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow,secondThrow);
		
		assertEquals(2,frame.getFirstThrow());
	}
	
	@Test
	public void secondThrowShouldBeFour() throws Exception{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow,secondThrow);
		
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void frameShouldBeInvalidForSecondThrowNegative() throws Exception{
		int firstThrow = 2;
		int secondThrow = -1;
		new Frame(firstThrow,secondThrow);
	}
	
	@Test(expected = BowlingException.class)
	public void frameShouldBeInvalidForFirstThrowNegative() throws Exception{
		int firstThrow = -1;
		int secondThrow = 4;
		new Frame(firstThrow,secondThrow);
	}
	
	@Test(expected = BowlingException.class)
	public void FrameShouldBeInvalidForFirstThrowMoreThanTen() throws Exception{
		int firstThrow = 11;
		int secondThrow = 4;
		new Frame(firstThrow,secondThrow);
	}
	
	@Test(expected = BowlingException.class)
	public void frameShouldBeInvalidForSecondThrowMoreThanTen() throws Exception{
		int firstThrow = 2;
		int secondThrow = 11;
		new Frame(firstThrow,secondThrow);
	}
	
	@Test
	public void frameScoreShouldBeEight() throws Exception{
		int firstThrow = 2;
		int secondThrow = 6;
		Frame frame = new Frame(firstThrow,secondThrow);
		
		assertEquals(8,frame.getScore());
	}
	
	@Test(expected = BowlingException.class)
	public void frameScoreShouldBeInvalid() throws Exception{
		int firstThrow = 5;
		int secondThrow = 6;
		new Frame(firstThrow,secondThrow);
	}
	
	@Test
	public void framesShouldBeSpare() throws Exception{
		Frame frame = new Frame(1,9);
		
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void bonusShouldBeZero() throws Exception{
		Frame frame = new Frame(1,9);
		frame.setBonus(0);
		
		assertEquals(0, frame.getBonus());
	}
	
	@Test
	public void scoreShouldBeEleven() throws Exception{
		Frame frame = new Frame(1,9);
		frame.setBonus(1);
		
		assertEquals(11, frame.getScore());
	}
	
	@Test
	public void framesShouldBeStrike() throws Exception{
		Frame frame = new Frame(10,0);
		
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void scoreShouldBeNineteen() throws Exception{
		Frame frame = new Frame(3,6);
		frame.setBonus(10);
		
		assertEquals(19, frame.getScore());
	}

}
