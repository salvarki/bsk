package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	
	private List<Frame>list = new ArrayList<Frame>();
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.list = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.list.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return list.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i = 0; i<list.size(); i++) {
			if(list.get(i).isStrike()) {
				if(list.get(list.size()-1) == list.get(i)) {
					firstBonusThrow = this.getFirstBonusThrow();
					secondBonusThrow = this.getSecondBonusThrow();
					if(firstBonusThrow == 10 && secondBonusThrow == 10) {
						list.get(i).setBonus(firstBonusThrow + secondBonusThrow+list.get(i).getFirstThrow());
					}else {
						list.get(i).setBonus(firstBonusThrow + secondBonusThrow);
					}
					
				}else if(i<list.size()-2 && list.get(i+1).isStrike()) {
					list.get(i).setBonus(list.get(i+1).getFirstThrow() + list.get(i+2).getFirstThrow());
				}else list.get(i).setBonus(list.get(i+1).getFirstThrow() + list.get(i+1).getSecondThrow());
			}else if(list.get(i).isSpare()) {
				if(list.get(list.size()-1) == list.get(i)) {
					firstBonusThrow = this.getFirstBonusThrow();
					list.get(i).setBonus(firstBonusThrow);
				}else {
					list.get(i).setBonus(list.get(i+1).getFirstThrow());
				}
			}
			score += list.get(i).getScore(); 
		}
		return score;	
	}

}
